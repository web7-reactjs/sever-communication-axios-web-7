import React from "react";
import Carousel from "react-bootstrap/Carousel";
import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
const ProductCard = () => {
  return (
    <>
      <Card style={{ width: "18rem" }}>
        <Carousel className="w-100">
          <Carousel.Item interval={500}>
            <img
              className="d-block w-100"
              src={
                "https://developers.elementor.com/docs/assets/img/elementor-placeholder-image.png"
              }
              alt="Second slide"
            />
          </Carousel.Item>
          <Carousel.Item interval={500}>
            <img
              className="d-block w-100"
              src={
                "https://thephotoimages.com/wp-content/uploads/2020/05/love-profile-pic.jpg"
              }
              alt="Second slide"
            />
          </Carousel.Item>
          <Carousel.Item>
            <img
              className="d-block w-100"
              src={
                "https://i.pinimg.com/736x/62/ab/dd/62abdd395246ad51481c162b985a61a0.jpg"
              }
              alt="Second slide"
            />
          </Carousel.Item>
        </Carousel>
        <Card.Body>
          <div className="d-flex justify-content-between align-items-center">
            <Card.Title>Card Title</Card.Title>
            <strong className="text-warning h2"> 120 $</strong>
          </div>
        
        <p> <strong> Shoes </strong></p>

          <Card.Text>
            Some quick example text to build on the card title and make up the
            bulk of the card's content.
          </Card.Text>
          <Button variant="primary">Go somewhere</Button>
        </Card.Body>
      </Card>
    </>
  );
};

export default ProductCard;
