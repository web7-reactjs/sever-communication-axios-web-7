import { Dropdown } from "bootstrap";
import React from "react";
import Container from "react-bootstrap/Container";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import NavDropdown from "react-bootstrap/NavDropdown";
import { NavLink } from "react-router-dom";
const MyNavBar = () => {
  return (
    <>
      <Navbar bg="light" expand="lg">
        <Container>
          <Navbar.Brand href="#home">React-Bootstrap</Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="me-auto">
              <NavDropdown title="Products" id="basic-nav-dropdown">
                <NavLink className="nav-link" to="/products">
                  {" "}
                  Products
                </NavLink>
                <NavLink className="nav-link" to="/insertProduct">
                  {" "}
                  Insert Products
                </NavLink>
              </NavDropdown>

              <NavDropdown title="Users" id="basic-nav-dropdown">
                <NavLink className="nav-link" to="/users">
                  {" "}
                  Users
                </NavLink>
                <NavLink className="nav-link" to="/insertUser">
                  {" "}
                  Insert User
                </NavLink>
              </NavDropdown>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </>
  );
};

export default MyNavBar;
