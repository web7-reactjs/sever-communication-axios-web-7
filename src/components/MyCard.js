import React from "react";

import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import { NavLink } from "react-router-dom";
const MyCard = ({ user }) => {
  // avatar
  // creationAt
  // email
  // id
  // name
  // password
  // role
  // updatedAt

  return (
    <>
      <Card style={{ width: "18rem" }}>
        <Card.Img
          variant="top"
          onError={({ currentTarget }) => {
            currentTarget.onerror = null;
            currentTarget.src =
              "https://upload.wikimedia.org/wikipedia/commons/7/7c/Profile_avatar_placeholder_large.png?20150327203541";
          }}
          src={
            user?.avatar
              ? user.avatar
              : "https://upload.wikimedia.org/wikipedia/commons/7/7c/Profile_avatar_placeholder_large.png?20150327203541"
          }
        />
        <Card.Body>
          <Card.Title>
            {" "}
            {user.name} <span className="text-warning"> {user.role}</span>{" "}
          </Card.Title>
          <Card.Text>
            <p> {user.email}</p>
            Some quick example text to build on the card title and make up the
            bulk of the card's content.
          </Card.Text>
          <div className="d-flex justify-content-between">

            <NavLink to={`/users/${user.id}`}>
              <Button variant="primary">View</Button>
            </NavLink>
            
            <Button variant="warning">Update</Button>
          </div>
        </Card.Body>
      </Card>
    </>
  );
};

export default MyCard;
