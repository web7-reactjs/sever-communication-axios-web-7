import logo from "./logo.svg";
import "./App.css";
import MyNavBar from "./components/MyNavBar";
import "bootstrap/dist/css/bootstrap.min.css";
import MyCard from "./components/MyCard";
import ProductCard from "./components/ProductCard";
import NotFoundPage from "./pages/NotFoundPage";
import InsertProduct from "./pages/InsertProduct";
import InsertUser from "./pages/InsertUser";
import{ Routes , Route} from "react-router-dom"
import AllProducts from "./pages/AllProducts"
import AllUsers from "./pages/AllUsers"
import ViewProfile from "./pages/ViewProfile";
function App() {
  return (
    <div>
      <MyNavBar />
  
      <Routes>

         <Route index element={<AllProducts/>}/>
         <Route path="products" element={<AllProducts/>}/>
         <Route path="users" element={<AllUsers/>}/>
         <Route path="users/:userid" element={<ViewProfile/>}/>
         <Route path="/insertUser" element={<InsertUser/>}/>
      </Routes>
       
    </div>
  );
}

export default App;
