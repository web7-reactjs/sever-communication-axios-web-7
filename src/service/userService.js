import { api } from "../utils/api";

// create service that works with the user api 
// get All user 

export const GET_ALL_USERS = async()=>{
    let response = await api.get("/users")
    return response.data
}

export const   GET_USER_BYID = async(id)=>{
    let response  = await api.get(`/users/${id}`)
    return response.data 
}


export const CREATE_USER = async(user)=>{
    let response = await api.post("/users",user)
    return response.data
}