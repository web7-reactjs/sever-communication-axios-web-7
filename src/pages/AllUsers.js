import React from "react";
import { useEffect, useState } from "react";
import { GET_ALL_USERS } from "../service/userService";
import MyCard from "../components/MyCard";

const AllUsers = () => {
  const [users, setUsers] = useState([]);

  useEffect(() => {
    GET_ALL_USERS()
      .then((response) => {
        setUsers(response);
      })
      .catch((err) => console.log("Errors : " + err));
  }, []);

  /**
    * 
    * 
    * avatar
creationAt
email
id
name
password
role
updatedAt
 
    */

  return (
    <>
      <div className="container">
        <div className="row">
          {users.map((user) => (
            <div className="col">
              <MyCard user={user} />
            </div>
          ))}
        </div>
      </div>
    </>
  );
};

export default AllUsers;
