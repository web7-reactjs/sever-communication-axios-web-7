import React from "react";
import { NavLink, useParams } from "react-router-dom";
import { useEffect, useState } from "react";
import { GET_USER_BYID } from "../service/userService";
const ViewProfile = () => {
  const [user, setUser] = useState({});
  const { userid } = useParams();

  useEffect(() => {
    // fetch user by id
    GET_USER_BYID(userid)
      .then((response) => setUser(response))
      .catch((err) => console.log("Error : ", err));
  }, []);
  return (
    <>
      <div className="container d-flex bg-light gap-5 mt-5 py-5 rounded-5 justify-content-center ">
        <div className="profile w-25 ">
          <img
            className="object-fit-contain w-100"
            onError={({currentTarget})=>{
              currentTarget.onerror = null; 
              currentTarget.src = "https://www.pngkey.com/png/full/73-730477_first-name-profile-image-placeholder-png.png"
      
            }}
            src={
              user.avatar
                ? user.avatar
                : "https://www.pngkey.com/png/full/73-730477_first-name-profile-image-placeholder-png.png"
            }
            alt="user profile image"
          />
          <input className="form-control" type="file" />
        </div>
        <div className=" ">
          <div>
            <h1>Profile Information</h1>
            <div className="d-flex mt-5 justify-content-center align-items-center gap-3">
              <label htmlFor=""> Name </label>
              <input type="text" value={user.name} className="form-control w-50" />

              <label htmlFor="roleSelector"> Role</label>

              <select
                className="form-control"
                name="roleSelector"
                id="roleSelector"
                value={user.role}
              >
                <option value="admin"> Admin</option>
                <option value="customer">Customer</option>
              </select>
            </div>
            <div className="d-flex justify-content-center align-items-center mt-3 gap-3">
              <label htmlFor=""> Email</label>
              <input type="text" value={user.email} className="form-control w-100" />
            </div>
            <div className="d-flex  align-items-center mt-3 gap-3">
              <label htmlFor=""> Password</label>
              <input type="text" value={user.password} className="form-control  " />
            </div>

            <div className="d-flex mt-4 justify-content-between ">
              <button>Update</button>
              
              <NavLink className="nav-link" to="/users">
                <button className="btn btn-danger"> Cancel </button>
              </NavLink>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default ViewProfile;
