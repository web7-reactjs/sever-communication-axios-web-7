import React from "react";
import { useState, useEffect } from "react";
import { CREATE_USER } from "../service/userService";
import { useNavigate } from "react-router-dom";

const InsertUser = () => {

  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [role, setRole] = useState("");
  const [avatar, setAvatar] = useState("");

  let navigate  = useNavigate(); 

  let user = {
    name,
    email,
    password,
    "role":"admin",
    avatar,
  };
 
  let canSave = (name && email && password &&   avatar) ? false : true;
  
  const createUser = (e)=>{
    e.preventDefault(); // anti-refreshing 
    CREATE_USER(user).then((response)=>{
      console.log("Created User : ",response)
      alert("Created User Successfully !")
      navigate("/users")
    }).catch(err=> console.log("Error inserting user : ",err))
    
  }
  return (
    <>
      <div className="container ">
        <div className="d-flex mt-5 rounded-5  bg-light  justify-content-center align-items-center gap-4">
          <div className="image w-50">
            <img
              className="object-fit-contain  w-100"
              alt=""
              src="https://cdn3d.iconscout.com/3d/premium/thumb/man-holding-sign-up-form-2937684-2426382.png"
            />
          </div>
          <div className="form-side  ">
            <div className="   mt-4 rounded-5 pt-5 px-3 pb-2">
              <h1> Create New Users </h1>

              <div className="my-5  ">
                <form action="">
                  <label htmlFor="inputName"> Username </label>
                  <input
                    id="inputName"
                    type="text"
                    name="inputName"
                    placeholder="Enter username "
                    className="form-control"
                    value={name}
                    onChange={(e) => setName(e.target.value)}
                  />
                  <label htmlFor="inputEmail"> Email</label>
                  <input
                    id="inputEmail"
                    name="inputEmail"
                    type="text"
                    placeholder="Enter email "
                    className="form-control"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                  />
                  <label htmlFor="inputPassword"> Password </label>
                  <input
                    type="password"
                    id="inputPassword"
                    name="inputPassword"
                    placeholder="Enter password "
                    className="form-control"
                    value={password}
                    onChange={(e)=> setPassword(e.target.value)}
                  />
                  <label htmlFor="inputAvatar"> Avatar </label>
                  <input
                    type="text"
                    id="inputAvatar"
                    name="inputAvatar"
                    placeholder="Enter Avatar Links "
                    className="form-control"
                    value={avatar}
                    onChange= {(e)=> setAvatar(e.target.value)}
                  />

                  <button
                    disabled={canSave}
                    className="
                btn
                 btn-warning 
                 my-2"
                 onClick={createUser}
                  >
                    Create Users
                  </button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default InsertUser;
