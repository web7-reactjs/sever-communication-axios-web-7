import React from 'react'

const InsertProduct = () => {
  return (
   <>
   <h1> inserting the product </h1>
   <div className="container d-flex justify-content-center bg-light mt-5 pt-5 pb-5 rounded-4  ">
        <div className="row g-5 ">
          <div className="productimage  col ">
            <img
              width="500px"
              className=" object-fit-contain"
              src={
                "https://developers.elementor.com/docs/assets/img/elementor-placeholder-image.png"
              }
              alt=""
            />
            <input
             
              type="file"
              className="form-control"
            />
          </div>

          <div className="productInformation col  ">
            <h1> Product Information</h1>
            <form>
              <label htmlFor="inputName"> Product Name </label>
              <input
                type="text"
             
                className="form-control"
              />

              <div className="container    px-0 d-flex justify-content-between">
                <div className="px-0">
                  <label htmlFor="inputPrice"> Product Price </label>
                  <input
                    type="number"
                  
                    className="form-control"
                  />
                </div>

                <div className="px-0">
                  <label htmlFor=""> Choose Category </label>
                  <select
                  
                    className="form-control"
                    name=""
                    id=""
                  >
                   
                      <option  >
                         Category One 
                      </option>
                      <option  >
                         Category Two
                      </option>
                   
                  </select>
                </div>
              </div>

              <label htmlFor="inputDescription">Product Description </label>
              <textarea
                type="text"
              
              
                className="form-control"
              />

              <div className="container mt-4 d-flex justify-content-between">
                {/* */}
                <button
                  
                  className="btn btn-warning "
                >
                  {" "}
                  Create Product{" "}
                </button>
                <div>
                  <button  className="btn btn-danger">
                    {" "}
                    Clear{" "}
                  </button>
               
                </div>
              </div>
            </form>
            
          </div>
        </div>
      </div>
   </>
  )
}

export default InsertProduct